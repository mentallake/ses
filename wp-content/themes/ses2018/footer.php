<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link
 *
 * @package WordPress
 * @subpackage ies
 * @since 1.0
 * @version 1.0
 */

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;

$logo = get_field('logo', $contact_page_id);
$footer_description = get_field('footer_description', $contact_page_id);
$copyright_text = get_field('copyright_text', $contact_page_id);
?>
		<a id="go-to-top-btn"><i class="fa fa-angle-up"></i></a>

		<div id="footer-panel">
			<div class="container">
				<div class="row">
					<div class="col-sm-2 col-md-3">
						<img id="footer-logo" src="<?php echo $logo; ?>" alt="">
					</div>
					<div class="col-sm-7 col-md-6">
						<?php
						wp_nav_menu( array(
			                'depth'             => 2,
			                'menu_class'        => 'nav',
			                'container'         => 'div',
			                'container_class'   => '',
			                'container_id'      => 'footer-menus')
			            );
						?>

						<div id="footer-text">
							<?php echo $footer_description; ?>
						</div>

						<div id="copyright" class="hidden-xs">
							© <?php echo date("Y"); ?> Copyright. <?php echo $copyright_text; ?>
						</div>
					</div>
					<div class="col-sm-3">
						<ul id="footer-contact" class="contact-list small">
							<li>
								<div class="list-icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="list-text">
									<?php the_field('phone', $contact_page_id); ?>
								</div>
							</li>
							<li>
								<div class="list-icon">
									<i class="fa fa-envelope-o"></i>
								</div>
								<div class="list-text">
									<?php the_field('email', $contact_page_id); ?>
								</div>
							</li>
							<?php
							while( have_rows('contacts', $contact_page_id) ): the_row();
								$icon = get_sub_field('icon');
								$title = get_sub_field('contact_title');
								$detail = get_sub_field('contact_detail');
							?>
							<li>
								<div class="list-icon">
									<img src="<?php echo $icon; ?>" alt="">
								</div>
								<div class="list-text">
									<?php echo $detail; ?>
								</div>
							</li>
							<?php endwhile; ?>
						</ul>

						<div id="mobile-copyright" class="copyright visible-xs">
							© <?php echo date("Y"); ?> Copyright. <?php echo $copyright_text; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="message-dialog" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title title-text-6">
                        	<img class="logo" src="<?php echo $logo; ?>" alt="">
                        </h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer text-center-force">
                        <button type="button" id="ok-button" data-dismiss="modal" class="btn btn-red-1 size-2 big">Dismiss</button>
                    </div>
                </div>
            </div>
        </div>

		<?php wp_footer(); ?>
	</body>
</html>