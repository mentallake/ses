<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ses
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$images = get_field('images');
$slide_title = get_field('slide_title');
$slide_description = get_field('slide_description');
$slide_button_text = get_field('slide_button_text');
$slide_button_link = get_field('slide_button_link');

$greeting_title = get_field('greeting_title');
$greeting_description = get_field('greeting_description');
$greeting_button_text = get_field('greeting_button_text');
$greeting_button_link = get_field('greeting_button_link');

$service_title = get_field('service_title');
$service_description = get_field('service_description');
$service_link_text = get_field('service_link_text');
$service_link = get_field('service_link');

// Get service page
$service_page = get_page_by_path( 'service' );
$service_page_id = $service_page->ID;
$services = array();

if( have_rows('services', $service_page_id) ){
	while( have_rows('services', $service_page_id) ): the_row();
		$image = get_sub_field('image');
		$title = get_sub_field('title');
		$item = array("image" => $image, "title" => $title);

		$services[] = $item;
	endwhile;
}

// Get faq page
$home_faq_title = get_field('home_faq_title');
$home_faq_description = get_field('home_faq_description');
$home_faq_link_text = get_field('faq_link_text');
$home_faq_link = get_field('faq_link');

$faq_page = get_page_by_path( 'faq' );
$faq_page_id = $faq_page->ID;
$questions = array();

if( have_rows('questions', $faq_page_id) ){
	while( have_rows('questions', $faq_page_id) ): the_row();
		$show_in_home_page = get_sub_field('show_in_home_page');
		$activate_question = get_sub_field('activate_question');

		if(!$activate_question || !$show_in_home_page){
			continue;
		}

		$question = get_sub_field('question');
		$answer = get_sub_field('answer');

		$item = array("question" => $question, "answer" => $answer);

		$questions[] = $item;
	endwhile;
}

// Call to action
$cta_title = get_field('cta_title');
$cta_subtitle = get_field('cta_subtitle');
$cta_button_text = get_field('cta_button_text');
$cta_button_link = get_field('cta_button_link');
?>

<div id="home-page">
	<section id="slides">
		<div id="slide-text">
			<div class="container">
				<div class="content-wrapper">
					<div class="title"><?php echo $slide_title; ?></div>
					<div class="subtitle">
						<?php echo $slide_description; ?>
					</div>
					<br>
					<a href="<?php echo $slide_button_link; ?>" class="btn btn-red-1 big"><?php echo $slide_button_text; ?></a>
				</div>
			</div>
		</div>

		<?php if( $images ){ ?>
		<div class="owl-carousel owl-theme">
			<?php foreach( $images as $image ): ?>
			<div class="item">
				<div class="item-content" style="background-image: url(<?php echo $image['url']; ?>);">
				</div>
			</div>
			<?php endforeach; ?>
		</div>
		<?php } ?>
	</section>

	<div class="content-page">
		<section id="greeting">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<h1 class="section-title"><?php echo $greeting_title; ?></h1>
						<div class="">
							<?php echo $greeting_description; ?>
							<br>
							<div class="">
								<a href="<?php echo $greeting_button_link; ?>" class="btn btn-red-1 big"><?php echo $greeting_button_text; ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="services" class="big-top-space">
			<div class="container">
				<div class="text-center">
					<div class="section-title"><?php echo $service_title; ?></div>
					<div>
						<?php echo $service_description; ?>
					</div>
					<div class="">
						<a href="<?php echo $service_link; ?>" class="link"><?php echo $service_link_text; ?></a>
					</div>
				</div>
				<br><br>
				<div class="row">
					<?php
					for($i = 0; $i < count($services); $i++){
						$image = $services[$i]["image"];
						$title = $services[$i]["title"];
					?>
					<div class="service-col col-sm-4">
						<div class="image-container square with-text" style="background-image: url(<?php echo $image; ?>);">
							<div class="title"><?php echo $title; ?></div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
		<section id="home-faq" class="big-top-space big-bottom-space">
			<div class="container">
				<div class="row">
					<div class="col-sm-5 hide">
						<div class="hide image-container square" style="background-image: url(<?php echo get_template_directory_uri() . '/images/spouse.jpg'; ?>);">
						</div>
					</div>
					<div class="col-sm-8 col-sm-offset-2">
						<div class="text-center">
							<div class="section-title"><?php echo $home_faq_title; ?></div>
							<div>
								<?php echo $home_faq_description; ?>
							</div>
							<div class="">
								<a href="<?php echo $home_faq_link; ?>" class="link"><?php echo $home_faq_link_text; ?></a>
							</div>
						</div>

						<div class="panel-group" id="faq-accordion" role="tablist" aria-multiselectable="true">
							<?php
							for($i = 0; $i < count($questions); $i++){
								$question = $questions[$i]['question'];
								$answer = $questions[$i]['answer'];

								$q_collapse_class = ($i == 0) ? "" : "collapsed";
								$a_collspse_class = ($i == 0) ? "in" : "";
							?>
						  	<div class="panel panel-default">
						    	<div class="panel-heading" role="tab" id="q-<?php echo $i + 1; ?>">
						      		<h4 class="panel-title">
						        		<a class="<?php echo $q_collapse_class; ?>" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#a-<?php echo $i + 1; ?>" aria-expanded="true" aria-controls="#a-<?php echo $i + 1; ?>">
						          			<?php echo $question; ?>
						        		</a>
						      		</h4>
						    	</div>
						    	<div id="a-<?php echo $i + 1; ?>" class="panel-collapse collapse <?php echo $a_collspse_class; ?>" role="tabpanel" aria-labelledby="q-<?php echo $i + 1; ?>">
						      		<div class="panel-body">
						      			<?php echo $answer; ?>
						      		</div>
						    	</div>
						  	</div>
						  	<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container">
				<div id="home-contact-us">
					<div class="row">
						<div class="col-sm-8 left-col">
							<div class="content">
								<div class="title">
									<?php echo $cta_title; ?>
								</div>
								<div class="subtitle">
									<?php echo $cta_subtitle; ?>
								</div>
							</div>
						</div>
						<div class="col-sm-4 right-col">
							<a href="<?php echo $cta_button_link; ?>" class="btn btn-white btn-skelleton big"><?php echo $cta_button_text; ?></a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php get_footer();