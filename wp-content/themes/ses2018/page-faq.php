<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ses
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$cover_image = get_field('faq_cover_image');
$page_title = get_field('faq_title');
$page_subtitle = get_field('faq_subtitle');
?>

<div id="faq-page">
	<div class="page-cover" style="background-image: url(<?php echo $cover_image; ?>);">
		<div class="cover-content">
			<div class="title"><?php echo $page_title; ?></div>
			<div class="subtitle">
				<?php echo $page_subtitle; ?>
			</div>
		</div>
	</div>

	<div class="container content-page">
		<?php if( have_rows('questions') ): ?>
		<div id="faq-panel" class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
			<div class="panel-group" id="faq-accordion" role="tablist" aria-multiselectable="true">
				<?php
				$count = 0;

				while( have_rows('questions') ): the_row();
					$activate_question = get_sub_field('activate_question');

					if(!$activate_question){
						continue;
					}

					$question = get_sub_field('question');
					$answer = get_sub_field('answer');
					$count++;

					$q_collapse_class = ($count == 1) ? "" : "collapsed";
					$a_collspse_class = ($count == 1) ? "in" : "";
				?>
			  	<div class="panel panel-default">
			    	<div class="panel-heading" role="tab" id="q-<?php echo $count; ?>">
			      		<h4 class="panel-title">
			        		<a class="<?php echo $q_collapse_class; ?>" role="button" data-toggle="collapse" data-parent="#faq-accordion" href="#a-<?php echo $count; ?>" aria-expanded="true" aria-controls="#a-<?php echo $count; ?>">
			          			<?php echo $question; ?>
			        		</a>
			      		</h4>
			    	</div>
			    	<div id="a-<?php echo $count; ?>" class="panel-collapse collapse <?php echo $a_collspse_class; ?>" role="tabpanel" aria-labelledby="q-<?php echo $count; ?>">
			      		<div class="panel-body">
			      			<?php echo $answer; ?>
			      		</div>
			    	</div>
			  	</div>
			  	<?php endwhile; ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php get_footer();