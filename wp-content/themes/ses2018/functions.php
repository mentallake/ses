<?php
require_once('libs/wp_bootstrap_navwalker.php');

function add_scripts(){
	// Css
	wp_enqueue_style('reset-css', get_template_directory_uri() . '/css/reset.min.css');
	wp_enqueue_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
	wp_enqueue_style('font-awesome-css', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css');
    wp_enqueue_style('owl-carousel-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css');
    wp_enqueue_style('owl-carousel-theme-css', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css');
    wp_enqueue_style('nunito-font', 'https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700');
    wp_enqueue_style( 'style-css', get_template_directory_uri() . '/style.css' );

	// Libs
	wp_enqueue_script('modernizr-js', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');
	wp_enqueue_script('jquery-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js');
	wp_enqueue_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js');
    wp_enqueue_script('owl-carousel-js', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js');
    wp_enqueue_script('jquery-form-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js');
    wp_enqueue_script('waypoint-js', 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js');
    wp_enqueue_script('animate-number-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-animateNumber/0.0.14/jquery.animateNumber.min.js');
    wp_enqueue_script('matchHeight-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js');
    wp_enqueue_script('recaptcha-js', 'https://www.google.com/recaptcha/api.js');

	// Scripts
	wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/app.js');

	if(is_page('home')){
		wp_enqueue_script('home-js', get_template_directory_uri() . '/scripts/home.js');
	}else if(is_page('about')){
		wp_enqueue_script('about-js', get_template_directory_uri() . '/scripts/about.js');
	}else if(is_page('service')){
		wp_enqueue_script('service-js', get_template_directory_uri() . '/scripts/service.js');
	}else if(is_page('faq')){
		wp_enqueue_script('faq-js', get_template_directory_uri() . '/scripts/faq.js');
	}else if(is_page('contact')){
		wp_enqueue_script('contact-js', get_template_directory_uri() . '/scripts/contact.js');
	}
}

function add_local_scripts(){
	wp_enqueue_style('reset-css', get_template_directory_uri() . '/css/reset.min.css');
	wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/fonts/font-awesome-4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/libs/bootstrap-3.3.7/css/bootstrap.min.css');
	wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/assets/owl.carousel.min.css');
	wp_enqueue_style('owl-carousel-theme-css', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/assets/owl.theme.default.min.css');
	wp_enqueue_style('nunito-font', 'https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700');
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/style.css' );

	// Libs
	wp_enqueue_script('modernizr-js', get_template_directory_uri() . '/libs/modernizr-2.8.3.min.js');
	wp_enqueue_script('jquery-js', get_template_directory_uri() . '/libs/jquery-1.12.4.min.js');
	wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/libs/bootstrap-3.3.7/js/bootstrap.min.js');
	wp_enqueue_script('owl-carousel-js', get_template_directory_uri() . '/libs/OwlCarousel2-2.2.1/owl.carousel.min.js');
	wp_enqueue_script('jquery-form-js', get_template_directory_uri() . '/libs/jquery.form.min.js');
	wp_enqueue_script('waypoint-js', get_template_directory_uri() . '/libs/jquery.waypoint.min.js');
	wp_enqueue_script('animate-number-js', get_template_directory_uri() . '/libs/jquery.animateNumber.min.js');
	wp_enqueue_script('matchHeight-js', get_template_directory_uri() . '/libs/matchHeight/jquery.matchHeight-min.js');
	wp_enqueue_script('recaptcha-js', 'https://www.google.com/recaptcha/api.js');

	// Scripts
	wp_enqueue_script('app-js', get_template_directory_uri() . '/scripts/app.js');

	if(is_page('home')){
		wp_enqueue_script('home-js', get_template_directory_uri() . '/scripts/home.js');
	}else if(is_page('about')){
		wp_enqueue_script('about-js', get_template_directory_uri() . '/scripts/about.js');
	}else if(is_page('service')){
		wp_enqueue_script('service-js', get_template_directory_uri() . '/scripts/service.js');
	}else if(is_page('faq')){
		wp_enqueue_script('faq-js', get_template_directory_uri() . '/scripts/faq.js');
	}else if(is_page('contact')){
		wp_enqueue_script('contact-js', get_template_directory_uri() . '/scripts/contact.js');
	}
}

add_action( 'wp_enqueue_scripts', 'add_scripts' );
// add_action( 'wp_enqueue_scripts', 'add_local_scripts' );

// Add menus to WP Admin sidebar
add_theme_support( 'menus' );

// Hide admin bar at front end.
show_admin_bar( false );

// Set title
function set_title($data){
    global $post;

    if(isset($post->post_title)){
    	$data = $post->post_title . ' | SES Investigation Thailand';
    }else{
    	$data = 'SES Investigation Thailand';
    }

    return $data;
}

add_filter('wp_title','set_title');

function cvf_mail_content_type() {
    return "text/html";
}

add_filter ("wp_mail_content_type", "cvf_mail_content_type");