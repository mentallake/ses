<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ses
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$cover_image = get_field('service_cover_image');
$page_title = get_field('service_title');
$page_subtitle = get_field('service_subtitle');
?>

<div id="service-page">
	<div class="page-cover" style="background-image: url(<?php echo $cover_image; ?>);">
		<div class="cover-content">
			<div class="title"><?php echo $page_title; ?></div>
			<div class="subtitle">
				<?php echo $page_subtitle; ?>
			</div>
		</div>
	</div>

	<div class="container content-page">
		<?php
		if( have_rows('services') ):
			$count = 0;

			while( have_rows('services') ): the_row();
				$image = get_sub_field('image');
				$title = get_sub_field('title');
				$description = get_sub_field('description');
				$count++;

				$col_push_class = ($count % 2 == 0) ? "col-sm-push-6" : "";
				$col_pull_class = ($count % 2 == 0) ? "col-sm-pull-4" : "";
		?>
		<div class="service-box">
			<div class="row">
				<div class="col-sm-offset-1 col-sm-4 <?php echo $col_push_class; ?>">
					<div class="image-container square" style="background-image: url(<?php echo $image; ?>);"></div>
				</div>
				<div class="col-sm-6 <?php echo $col_pull_class; ?>">
					<div class="service-content">
						<div class="title"><?php echo $title; ?></div>
						<div class="text">
							<?php echo $description; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer();