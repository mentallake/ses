<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ses
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$cover_image = get_field('cover_image');
$page_title = get_field('title');
$page_subtitle = get_field('subtitle');

$cf_name_label = get_field('cf_name_label');
$cf_name_placeholder = get_field('cf_name_placeholder');
$cf_phone_label = get_field('cf_phone_label');
$cf_phone_placeholder = get_field('cf_phone_placeholder');
$cf_email_label = get_field('cf_email_label');
$cf_email_placeholder = get_field('cf_email_placeholder');
$cf_message_label = get_field('cf_message_label');
$cf_message_placeholder = get_field('cf_message_placeholder');

$contact_description = get_field('contact_description');

// Get contact page id
$contact_page = get_page_by_path( 'contact' );
$contact_page_id = $contact_page->ID;
$contact_page_url = get_page_link($contact_page_id);
?>

<div id="contact-page">
	<div class="page-cover" style="background-image: url(<?php echo $cover_image; ?>);">
		<div class="cover-content">
			<div class="title"><?php echo $page_title; ?></div>
			<div class="subtitle">
				<?php echo $page_subtitle; ?>
			</div>
		</div>
	</div>
	<div class="container content-page">
		<section>
			<div class="row">
				<div class="col-sm-6">
					<form id="contact-form" method="post" action="<?php echo get_template_directory_uri() . "/contact_submitted.php"; ?>">
						<input type="hidden" id="contact-page-url" value="<?php echo $contact_page_url; ?>">

				  		<div class="form-group">
				    		<label for="name-textbox"><?php echo $cf_name_label; ?></label>
				    		<input type="text" name="contact_name" class="form-control required" id="name-textbox" placeholder="<?php echo $cf_name_placeholder; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="mobile-textbox"><?php echo $cf_phone_label; ?></label>
				    		<input type="tel" name="contact_mobile" class="phone-keyboard-only form-control required" id="mobile-textbox" placeholder="<?php echo $cf_phone_placeholder; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="email-textbox"><?php echo $cf_email_label; ?></label>
				    		<input type="email" name="contact_email" class="form-control required" id="email-textbox" placeholder="<?php echo $cf_email_placeholder; ?>">
				  		</div>
				  		<div class="form-group">
				    		<label for="message-textarea"><?php echo $cf_message_label; ?></label>
				    		<textarea name="contact_message" class="form-control required" rows="5" placeholder="<?php echo $cf_message_placeholder; ?>"></textarea>
				  		</div>
				  		<div class="g-recaptcha"
				  		  	 data-sitekey="6LebwkwUAAAAAJsnzD3hiXnJ9WVnNC9Hj1VgkiGj"
				  		  	 data-callback="enableBtn"></div>
				  		<br>
				  		<div class="button-panel text-center">
				  			<button type="submit" id="submit-contact-btn" class="btn btn-red-1 big">Send</button>
				  		</div>
				  	</form>
				</div>
				<div class="col-sm-5 col-sm-offset-1">
					<ul id="contact-list" class="contact-list">
						<li>
							<div class="list-icon">
								<i class="fa fa-phone"></i>
							</div>
							<div class="list-text">
								<div class="list-label"><?php echo $cf_phone_label; ?></div>
								<?php the_field('phone'); ?>
							</div>
						</li>
						<li>
							<div class="list-icon">
								<i class="fa fa-envelope-o"></i>
							</div>
							<div class="list-text">
								<div class="list-label"><?php echo $cf_email_label; ?></div>
								<?php the_field('email'); ?>
							</div>
						</li>
						<?php
						while( have_rows('contacts') ): the_row();
							$icon = get_sub_field('icon');
							$title = get_sub_field('contact_title');
							$detail = get_sub_field('contact_detail');
						?>
						<li>
							<div class="list-icon">
								<img src="<?php echo $icon; ?>" alt="">
							</div>
							<div class="list-text">
								<div class="list-label"><?php echo $title; ?></div>
								<?php echo $detail; ?>
							</div>
						</li>
						<?php endwhile; ?>
					</ul>

					<div>
						<?php echo $contact_description; ?>
					</div>
				</div>
			</div>
		</section>
	</div>
	<div class="container">
		<div id="credit">Communication icon by <a href="https://icons8.com">Icons8</a></div>
	</div>
</div>

<?php get_footer();