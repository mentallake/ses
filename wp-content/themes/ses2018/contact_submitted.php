<?php
require_once("../../../wp-load.php");

// if the submit button is clicked, send the email
if ( isset( $_POST['contact_name'] ) ) {
    // sanitize form values
    $name = sanitize_text_field($_POST["contact_name"]);
    $mobile = sanitize_text_field($_POST["contact_mobile"]);
    $email = sanitize_email($_POST["contact_email"]);
    $message = esc_textarea($_POST["contact_message"]);
    $contact_page_url = $_POST["contact_page_url"];

    // Recaptcha
    $recaptcha_response = $_POST["g-recaptcha-response"];

    $post_data = http_build_query(
        array(
            'secret' => '6LebwkwUAAAAAOgLSMwi3y1pILKeThBDVXFtfqBS',
            'response' => $recaptcha_response,
            'remoteip' => $_SERVER['REMOTE_ADDR']
        )
    );

    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $post_data
        )
    );

    $context  = stream_context_create($opts);
    $verify_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    $verify_result = json_decode($verify_response);

    if (!$verify_result->success) {
        $result = false;
        $message = "Verification failed, please try submiting form again or call me directly.<br><br>Sorry for your inconvenience.";

        echo json_encode(array(
            "result" => $result,
            "message" => $message,
            "redirect_url" => $contact_page_url
        ));
        return;
    }

    // Insert database
    global $wpdb;
    $table_name = $wpdb->prefix . "contact";

    $wpdb->insert($table_name, array(
                    'name' => $name,
                    'email' => $email,
                    'mobile' => $mobile,
                    'message' => $message,
                )
            );

    $mail_subject = "[SES] " . $name . " has submitted contact form";

    // Html
    $mail_message = "Here is an auto mail from SES website. Someone has just submitted the contact form.<br><br>";
    $mail_message .= "<table><tbody>";
    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Name:</td>";
    $mail_message .= "<td>$name</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Mobile:</td>";
    $mail_message .= "<td>$mobile</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Email:</td>";
    $mail_message .= "<td>$email</td>";
    $mail_message .= "</tr>";

    $mail_message .= "<tr>";
    $mail_message .= "<td style='font-weight: bold;' align='right' valign='top'>Message:</td>";
    $mail_message .= "<td>" . nl2br($message) . "</td>";
    $mail_message .= "</tr>";
    $mail_message .= "</tbody></table>";

    // get the blog administrator's email address
    $contact_page = get_page_by_path( 'contact' );
    $contact_page_id = $contact_page->ID;
    $to_email = get_field('email', $contact_page_id);

    $to = $to_email;

    // $headers = "From: C In Website <noreply@cinsolutions.co.th>\r\n";
    // $headers .= "Return-Path: noreply@cinsolutions.co.th";
    $headers = "MIME-Version: 1.0";
    $headers .= "Content-Type: text/html; charset=UTF-8";

    // If email has been process for sending, display a success message
    if ( wp_mail( $to, $mail_subject, $mail_message, $headers ) ) {
        $result = true;
        $result_message = "Thank you $name for contacting us, we will reach you soon.";
    } else {
        $result = false;
        $result_message = "Something wrong happened, we could not receive your message. You could call us directly.<br><br>Sorry for your inconvenience.";
    }

    $redirect_url = $contact_page_url;

    echo json_encode(array(
        "result" => $result,
        "message" => $result_message,
        "redirect_url" => $redirect_url
    ));
}else{
    $result = false;
    $result_message = "No any submission";

    echo json_encode(array(
        "result" => $result,
        "message" => $result_message
    ));
}
?>