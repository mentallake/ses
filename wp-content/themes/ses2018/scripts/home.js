$(function(){
	$(".owl-carousel").owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        nav: false,
        dots: false,
        navSpeed : 500,
        dotSpeed : 500,
        navText: '',
        autoHeight: false,
        autoplay: true,
        autoplayTimeout: 3500,
        animateOut: 'fadeOut'
    });
});