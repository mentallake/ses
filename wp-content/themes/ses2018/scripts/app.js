$(function(){
	initAnimateCss();
	initElements();
});

function initElements(){
    $(window).on('load', function(){
        positionLanguageSwitcher();
    });

    $(window).resize(function(){
        positionLanguageSwitcher();
    });

	$(window).scroll(function(){
		var currentScroll = $(this).scrollTop();

	   	if (currentScroll >= 200){
	   		$('#go-to-top-btn').fadeIn();
	   	}else{
	   		$('#go-to-top-btn').fadeOut();
	   	}
	});

	$('#go-to-top-btn').click(function(){
		$('html, body').animate({
          	scrollTop: 0
        }, 1000);
	});

	$('.number-only').keypress(function(evt){
	    if (evt.which < 48 || evt.which > 57){
	        evt.preventDefault();
	    }
	});

	$('.phone-keyboard-only').keypress(function(e){
		// Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: #, *, +
            (e.keyCode == 35 || e.keyCode == 42 || e.keyCode == 43)) {
                 // let it happen, don't do anything
                 return;
        }

        // Ensure that it is a number and stop the keypress
        if (e.keyCode < 48 || e.keyCode > 57) {
            e.preventDefault();
        }
	});
}

function positionLanguageSwitcher(){
    var windowWidth = $(window).width();

    $('[href="#language-placeholder#"]').addClass('hide');

    if(windowWidth < 768){
        $('.navbar-brand').parent().append($('#qtranslate-chooser'));
        $('#qtranslate-chooser').css('display', 'inline-block');
        $('#qtranslate-chooser').css('float', 'right');
    }else{
        $('[href="#language-placeholder#"]').parent().append($('#qtranslate-chooser'));
        $('#qtranslate-chooser').css('float', 'none');
        $('#qtranslate-chooser').css('display', 'inline-block');
    }

    $('#footer-menus [href="#language-placeholder#"]').parent().addClass('hide');
}

function isFieldCompleted(panelId){
	var $panel = $(panelId);
	var isFieldCompleted = true;

	$panel.find('.error').removeClass('error');

	$panel.find('.required').each(function(){
        // If it is textbox, textarea.
        if($(this).is('input, textarea')){
            if($(this).val() == ''){
                isFieldCompleted = false;

                if($(this).parent().hasClass('input-file-wrapper')){
                	$(this).parent().addClass('error');
                }else{
                	$(this).addClass('error');
                }
            }
        }
    });

    $panel.find('[type="email"]').each(function(){
        // If it is email.
        if($(this).is('input')){
            if(!validateEmail($(this).val())){
                isFieldCompleted = false;

                if($(this).parent().hasClass('input-file-wrapper')){
                	$(this).parent().addClass('error');
                }else{
                	$(this).addClass('error');
                }
            }
        }
    });

    if(!isFieldCompleted){
        $panel.find('.error').eq(0).focus();
        $panel.find('.error').animateCss('pulse', 'in');
    }

    return isFieldCompleted;
}

function validateEmail(email) {
  	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  	return re.test(email);
}

function initAnimateCss(){
	$.fn.extend({
	    animateCss: function (animationName, type) {
	    	if(type == 'in'){
            	$(this).removeClass('hide');
            	$(this).css('opacity', '1');
            }

	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);

	            if(type == 'out'){
	            	$(this).css('opacity', '0');
	            	$(this).addClass('hide');
	            }
	        });
	    }
	});
}