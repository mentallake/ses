$(function(){
	$('#contact-form').submit(function(){
        if(!isFieldCompleted('#contact-form')){
            return false;
        }

		var contactPageUrl = $('#contact-page-url').val();
		var options = {
	        success: showResponse,
	        dataType: 'json',
	        data: { contact_page_url : contactPageUrl }
	    };

	    $(this).ajaxSubmit(options);

	    return false;
	});

    document.getElementById("submit-contact-btn").disabled = true;
});

function enableBtn(){
    document.getElementById("submit-contact-btn").disabled = false;
}

function showResponse(response, statusText, xhr, $form)  {
	if(response.result == false){
        var message = response.message;

        $('#message-dialog .modal-body').html(message);

        // hideLoadingPanel();

        // Display message dialog
        $('#message-dialog').modal();
    }else{
    	// hideLoadingPanel();

    	var message = response.message;
    	var redirectUrl = response.redirect_url;

    	$('#message-dialog').modal('hide');
        $('#message-dialog .modal-body').html(message);

        $('#message-dialog #ok-button').click(function(){
        	window.location.href = redirectUrl;
        });

        $('#message-dialog').modal();
    }
}