<?php
/**
 * The main template file
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage ses
 * @since 1.0
 * @version 1.0
 */

get_header();

global $post;

$cover_image = get_field('about_cover_image');
$page_title = get_field('about_title');
$page_subtitle = get_field('about_subtitle');

$who_we_are_title = get_field('who_we_are_title');
$who_we_are_description = get_field('who_we_are_description');

$why_choose_us_title = get_field('why_choose_us_title');
$why_choose_us_description = get_field('why_choose_us_description');

$our_past_clients_title = get_field('our_past_clients_title');
$our_past_clients_description = get_field('our_past_clients_description');
$button_text = get_field('button_text');
$button_link = get_field('button_link');
?>

<div id="about-page">
	<div class="page-cover" style="background-image: url(<?php echo $cover_image; ?>);">
		<div class="cover-content">
			<div class="title"><?php echo $page_title; ?></div>
			<div class="subtitle">
				<?php echo $page_subtitle; ?>
			</div>
		</div>
	</div>

	<div class="container content-page">
		<section>
			<div class="row">
				<div class="col-sm-7">
					<div class="page-title">
						<?php echo $who_we_are_title; ?>
					</div>
					<div>
						<?php echo $who_we_are_description; ?>
					</div>
				</div>
			</div>
		</section>
		<section id="">
			<div class="row">
				<div class="col-sm-12">
					<div class="section-title"><?php echo $why_choose_us_title; ?></div>
					<div>
						<?php echo $why_choose_us_description; ?>
					</div>
				</div>
			</div>
		</section>
		<section id="client-choose-panel" class="">
			<div class="row">
				<div class="col-sm-12">
					<div class="section-title"><?php echo $our_past_clients_title; ?></div>
					<div>
						<?php echo $our_past_clients_description; ?>
					</div>

					<br><br>
					<div class="text-center">
						<a href="<?php echo $button_link; ?>" class="btn btn-red-1 big"><?php echo $button_text; ?></a>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php get_footer();