<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ses_web');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'BiKyhbiO?dv$)Fp}/o5S*L0tn_3la =uf<pfV0FEO~8Xb[j=#Klep:Bojd?/^8u8');
define('SECURE_AUTH_KEY',  '=*OVqpxuyonUV&H0$SfM2?s-.9R/??.s}]`Inb4;T7wiGH[Olo+m5~%N&jYuShSo');
define('LOGGED_IN_KEY',    'bNPf~+u J*hW8_gA,r.xRPEKP_DR*6=^[*{,V2wf/h,{{/p4jxM$*){<&QYH(2O.');
define('NONCE_KEY',        'MPEoKaPD&g!&L$@](lUND^aocdc[,c90}G!knf[!$@.=l6Z(vr--=|Bs(}>c-F9K');
define('AUTH_SALT',        '^S]{Jh~=>!qI6zi%M[YfL S5=xQPeU^a2f/FJ]H=u&X!1]^YPd4/[7<VgBwYb}&h');
define('SECURE_AUTH_SALT', '}992[5dJB4lWcwl>*osIwxXxjl 8aqw?qPMiBR9;Z}dzF1*{``qMvr3(~^vE?``L');
define('LOGGED_IN_SALT',   '@6}GJy>ZF+7tcUb_q};+$v0mXRv,m|$:LQ+>6_QzVvsLa3DuR|Az)ZF43~ua>Yu<');
define('NONCE_SALT',       '55X[AbUhSDlU9..R%(4/|i@N8DX,x-Q,76QMjWu.M71+,v#-JPB0nyc}:6Mz^opB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ses_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
